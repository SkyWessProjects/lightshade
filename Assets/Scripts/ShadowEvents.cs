using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShadowEvents : MonoBehaviour
{
    public GameObject player;

    public float shadowtimer = 0f;
    public float gracePeriod = .5f;
    // Update is called once per frame
    void Update()
    {
    }

    public void ShadowStay()
    {
        shadowtimer = 0f;
        player.GetComponent<PlayerMovement>().enabled = true;
        player.GetComponentInChildren<Animator>().SetBool("OutOfShadow", false);
        Debug.Log("In a shadow");
    }
    public void ShadowOut()
    {
        shadowtimer += Time.deltaTime;
        if(shadowtimer > gracePeriod)
        {
            player.GetComponent<PlayerMovement>().enabled = false;
            player.GetComponentInChildren<Animator>().SetBool("OutOfShadow", true);
        }
        Debug.Log("Not in a shadow");
    }
}

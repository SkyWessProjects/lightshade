using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class StartAndPause : MonoBehaviour
{
    public Animator anim;
    private bool Player1Goal = false;
    private bool Player2Goal = false;
    public bool IsCredits = false;

    private void Start()
    {
        if(anim == null)
        anim = GetComponent<Animator>();
        if (IsCredits == true)
            anim.SetBool("IsCredits", true);
    }
    private void Update()
    {
        StartFunction();

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            PauseGame();
        }
    }
    // Update is called once per frame
    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Player1")
        {
            anim.SetTrigger("Tutorial");
            Player1Goal = true;
        }
        if (other.gameObject.tag == "Player2")
        {
            anim.SetTrigger("Tutorial");
            Player2Goal = true;
        }
        if(Player1Goal == true && Player2Goal == true)
            anim.SetTrigger("Load");
    }
    // Update is called once per frame
    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Player1")
        {
            Player1Goal = false;
        }
        if (other.gameObject.tag == "Player2")
        {
            Player2Goal = false;
        }
    }

    public void StartFunction()
    {
        if (Input.anyKeyDown && SceneManager.GetActiveScene().buildIndex == 0)
        {
            anim.SetTrigger("Load");
        }
        if(SceneManager.GetActiveScene().buildIndex == 1)
        {
            anim.SetTrigger("Tutorial");
        }
    }

    public void TriggerNextScene()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }
    
    public void PauseGame()
    {
        anim.SetTrigger("Pause");
    }
}

using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public float MoveSpeed = 3f;
    public float TurnSpeed = 3f;
    public float gravityValue = -9;
    public GameObject model;

    [SerializeField]
    private int playerIndex = 0;
    private Animator anim;

    private CharacterController controller;

    private Vector3 moveDirection = Vector3.zero;
    private Vector2 inputVector = Vector2.zero;


    private void Awake()
    {
        controller = GetComponent<CharacterController>();
        anim = GetComponentInChildren<Animator>();
    }

    public int GetPlayerIndex()
    {
        return playerIndex;
    }

    public void SetInputVector(Vector2 direction) 
    {
        inputVector = direction;
    }
    // Update is called once per frame
    void Update()
    {
        moveDirection = new Vector3(inputVector.x, 0, inputVector.y);
        if(moveDirection != Vector3.zero)
            model.transform.rotation = Quaternion.RotateTowards(model.transform.rotation, Quaternion.LookRotation(moveDirection, Vector3.up), TurnSpeed);
        moveDirection = transform.TransformDirection(moveDirection);
        moveDirection *= MoveSpeed;
        if (moveDirection != Vector3.zero)
            anim.SetBool("IsMoving", true);
        else
            anim.SetBool("IsMoving", false);

        moveDirection.y += gravityValue * Time.deltaTime;
        controller.Move(moveDirection * Time.deltaTime);
    }
}

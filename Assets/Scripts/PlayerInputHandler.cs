using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.InputSystem;
using static UnityEngine.InputSystem.InputAction;

public class PlayerInputHandler : MonoBehaviour
{
    private PlayerInput playerInput;
    private PlayerMovement mover;
    private PlayerMovement[] movers;
    private int i = 0;
    // Start is called before the first frame update
    private void Awake()
    {
        playerInput = GetComponent<PlayerInput>();
        movers = FindObjectsOfType<PlayerMovement>();
        var index = playerInput.playerIndex;
        mover = movers.FirstOrDefault(m => m.GetPlayerIndex() == index);
    }

    private void Update()
    {
        if(Input.GetKeyDown(KeyCode.Tab))
        {
            if(i == 0)
            {
                movers[i] = movers[0];
                movers[i+1] = movers[1];
                i = 1;
            }
            else if (i == 1)
            {
                movers[i] = movers[1];
                movers[i - 1] = movers[0];
                i = 0;
            }
            mover = movers[i];
        }
    }
    // Update is called once per frame
    public void OnMove(CallbackContext context)
    {
        mover.SetInputVector(context.ReadValue<Vector2>());
    }
}

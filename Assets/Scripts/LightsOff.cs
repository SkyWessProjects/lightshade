using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightsOff : MonoBehaviour
{
    private Animator anim;

    private void Awake()
    {
        anim = gameObject.GetComponent<Animator>();
    }
    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.tag == "Player1")
        {
            if (Input.GetKeyDown(KeyCode.E))
            {
                anim.SetTrigger("Switch");
            }
        }
    }
}
